<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Voiture;

class Facture
{
    private $texte;

    public function __construct(Voiture $voiture) {
        $this->texte = "Facture de ".$voiture->getPrix()."€ pour la voiture \"".$voiture->getModel()."\"";
    }

    public function getTexte() {
        return $this->texte;
    }

    public function setTexte(Voiture $voiture) {
        $this->texte = "Facture de ".$voiture->getPrix()."€ pour la voiture \"".$voiture->getModel()."\"";
    }
}