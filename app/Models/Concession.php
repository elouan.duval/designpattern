<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Voiture;

class Concession
{
    private $nom;
    private $tab_voiture;

    public function __construct($nom) {
        $this->tab_voiture = [];
        $this->nom = $nom;
    }

    public function getTabVoiture() {
        return $this->tab_voiture;
    }

    public function addTabVoiture(Voiture $voiture) {
        $this->tab_voiture[] = $voiture;
    }

    public function deleteTabVoiture(Voiture $voiture) {
        unset($tab[array_search($voiture, $this->tab_voiture)]);
    }

    public function getNom() {
        return $this->nom;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }
}