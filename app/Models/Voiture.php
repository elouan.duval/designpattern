<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

abstract class Voiture
{
    public abstract function getModel();
    public abstract function getPrix();

    public function toString() {
        return $this->getModel()." : ".$this->getPrix()."€";
    }
}