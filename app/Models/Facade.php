<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\VoitureFactory;
use App\Models\Concession;
use App\Models\Facture;

class Facade
{
    public static function commander($marque, $model, $prix, Concession $concession) {
        $voiture = VoitureFactory::getVoiture($marque, $model, $prix);
        $concession->addTabVoiture($voiture);
        $facture = new Facture($voiture);

        return ["usine" => $voiture, "concession" => $concession, "facture" => $facture];
    }
}