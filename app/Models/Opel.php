<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opel extends Voiture
{
    private $model;
    private $prix;

    public function __construct($model, $prix) {
        $this->model = $model;
        $this->prix = $prix;
    }

    public function getModel() {
        return $this->model;
    }

    public function getPrix() {
        return $this->prix;
    }
}