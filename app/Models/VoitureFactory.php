<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Opel;
use App\Models\Renault;

class VoitureFactory
{
    public static function getVoiture($marque, $model, $prix) {

        if(strtolower("Opel") == strtolower($marque)) {

            return new Opel($model, $prix);

        } 
        elseif(strtolower("Renault") == strtolower($marque)) {

            return new Renault($model, $prix);

        } 
        else {

            return null;
            
        }
    }
}
