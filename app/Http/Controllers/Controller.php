<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Singleton;
use App\Models\VoitureFactory;
use App\Models\Facade;
use App\Models\Concession;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function go()
    {
        $a = Singleton::getInstance();
        $b = Singleton::getInstance();

        return view('singleton', ['a'=>$a->increment(), 'b'=>$b->increment()]);
        
    }
    public function factory() {
        $opel1 = VoitureFactory::getVoiture("Opel", "Opel 1", 15000);
        $opel2 = VoitureFactory::getVoiture("Opel", "Opel 2", 10000);
        $renault1 = VoitureFactory::getVoiture("Renault", "Renault 1", 12000);
        $renault2 = VoitureFactory::getVoiture("Renault", "Renault 2", 9000);

        return view('factory', ['opel1' => $opel1,
                                'opel2' => $opel2,
                                'renault1' => $renault1,
                                'renault2' => $renault2]);
    }

    public function facade() {
        $rennes = new Concession("Concession Rennes");
        $lannion =  new Concession("Concession Lannion");
        $brest =  new Concession("Concession Brest");

        $facade1 = Facade::commander("Opel", "Opel 1", 15000, $rennes);
        $facade2 = Facade::commander("Renault", "Renault 1", 12000, $rennes);
        $facade3 = Facade::commander("Opel", "Opel 2", 10000, $lannion);
        $facade4 = Facade::commander("Renault", "Renault 2", 9000, $brest);

        $usines = [$facade1['usine'], $facade2['usine'], $facade3['usine'], $facade4['usine']];
        $factures = [$facade1['facture'], $facade2['facture'], $facade3['facture'], $facade4['facture']];
        $concessions = [$rennes, $lannion, $brest];

        return view('facade', ['usines' => $usines, 'concessions' => $concessions, 'factures' => $factures]);
    }

    public function observer(){
        
    }
}