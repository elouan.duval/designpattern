<!DOCTYPE html>
<html>
    <head>
        <title>Factory</title>
        <meta charset="utf-8">
    </head>
    <body>
        <p>{{ $opel1->toString() }}</p>
        <p>{{ $opel2->toString() }}</p>
        <p>{{ $renault1->toString() }}</p>
        <p>{{ $renault2->toString() }}</p>
    </body>
</html>