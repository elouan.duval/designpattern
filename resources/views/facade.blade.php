<!DOCTYPE html>
<html>
    <head>
        <title>Facade</title>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Usines</h2>
        @foreach ($usines as $usine)
            <p>{{ $usine->toString() }}</p>
        @endforeach
        <br>
        <h2>Concessions</h2>
        @foreach ($concessions as $concession)
            <p><strong>{{ $concession->getNom() }}</strong></p>
            <ul>
            @foreach ($concession->getTabVoiture() as $voiture)
                <li>{{ $voiture->toString() }}</li>
            @endforeach
            </ul>
        @endforeach
        <br>
        <h2>Factures</h2>
        @foreach ($factures as $facture)
            <p>{{ $facture->getTexte() }}</p>
        @endforeach
    </body>
</html>