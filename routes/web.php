<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/singleton', 'App\Http\Controllers\Controller@go');

Route::get('/factory', 'App\Http\Controllers\Controller@factory');

Route::get('/facade', 'App\Http\Controllers\Controller@facade');

Route::get('/observer', 'App\Http\Controllers\Controller@observer');